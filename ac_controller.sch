EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Screw_Terminal_01x03 J1
U 1 1 5F78EEE1
P 1600 3800
F 0 "J1" H 1518 3475 50  0000 C CNN
F 1 "AC_IN" H 1518 3566 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00068_1x03_P7.50mm_Horizontal" H 1600 3800 50  0001 C CNN
F 3 "~" H 1600 3800 50  0001 C CNN
	1    1600 3800
	-1   0    0    1   
$EndComp
$Comp
L power:GND1 #PWR0101
U 1 1 5F79312D
P 2200 4000
F 0 "#PWR0101" H 2200 3750 50  0001 C CNN
F 1 "GND1" H 2205 3827 50  0000 C CNN
F 2 "" H 2200 4000 50  0001 C CNN
F 3 "" H 2200 4000 50  0001 C CNN
	1    2200 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND2 #PWR0102
U 1 1 5F79336B
P 1950 4000
F 0 "#PWR0102" H 1950 3750 50  0001 C CNN
F 1 "GND2" H 1955 3827 50  0000 C CNN
F 2 "" H 1950 4000 50  0001 C CNN
F 3 "" H 1950 4000 50  0001 C CNN
	1    1950 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3800 2200 3800
Wire Wire Line
	2200 3800 2200 4000
Wire Wire Line
	1950 4000 1950 3900
Wire Wire Line
	1950 3900 1800 3900
$Comp
L Connector:Screw_Terminal_01x03 J2
U 1 1 5F796DCE
P 1600 4800
F 0 "J2" H 1518 4475 50  0000 C CNN
F 1 "AC_OUT" H 1518 4566 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00068_1x03_P7.50mm_Horizontal" H 1600 4800 50  0001 C CNN
F 3 "~" H 1600 4800 50  0001 C CNN
	1    1600 4800
	-1   0    0    1   
$EndComp
$Comp
L power:GND1 #PWR0103
U 1 1 5F796DD4
P 2050 5000
F 0 "#PWR0103" H 2050 4750 50  0001 C CNN
F 1 "GND1" H 2055 4827 50  0000 C CNN
F 2 "" H 2050 5000 50  0001 C CNN
F 3 "" H 2050 5000 50  0001 C CNN
	1    2050 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND2 #PWR0104
U 1 1 5F796DDA
P 2250 5000
F 0 "#PWR0104" H 2250 4750 50  0001 C CNN
F 1 "GND2" H 2255 4827 50  0000 C CNN
F 2 "" H 2250 5000 50  0001 C CNN
F 3 "" H 2250 5000 50  0001 C CNN
	1    2250 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4800 2050 5000
$Comp
L Device:R R2
U 1 1 5F79CE0F
P 2450 1750
F 0 "R2" V 2243 1750 50  0000 C CNN
F 1 "1M/300V" V 2334 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric_Pad1.52x2.65mm_HandSolder" V 2380 1750 50  0001 C CNN
F 3 "~" H 2450 1750 50  0001 C CNN
	1    2450 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F79F3B1
P 2450 2050
F 0 "R3" V 2243 2050 50  0000 C CNN
F 1 "330R/300V" V 2334 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric_Pad1.52x2.65mm_HandSolder" V 2380 2050 50  0001 C CNN
F 3 "~" H 2450 2050 50  0001 C CNN
	1    2450 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 1350 2050 1750
Wire Wire Line
	2300 1750 2050 1750
$Comp
L Isolator:SFH620A-1 U1
U 1 1 5F7A4FBC
P 3350 1950
F 0 "U1" H 3350 2317 50  0000 C CNN
F 1 "SFH620A-1" H 3350 2226 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W8.89mm_SMDSocket_LongPads" H 3350 1550 50  0001 C CNN
F 3 "http://www.vishay.com/docs/83675/sfh620a.pdf" H 3300 1950 50  0001 C CNN
	1    3350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1850 3050 1850
Wire Wire Line
	2900 1350 2900 1750
Wire Wire Line
	2600 1750 2900 1750
Connection ~ 2900 1750
Wire Wire Line
	2900 1750 2900 1850
Wire Wire Line
	2600 2050 3050 2050
$Comp
L power:GND #PWR0105
U 1 1 5F7ABC88
P 3850 2250
F 0 "#PWR0105" H 3850 2000 50  0001 C CNN
F 1 "GND" H 3855 2077 50  0000 C CNN
F 2 "" H 3850 2250 50  0001 C CNN
F 3 "" H 3850 2250 50  0001 C CNN
	1    3850 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2050 3850 2050
Wire Wire Line
	3850 2050 3850 2250
Wire Wire Line
	3650 1850 3800 1850
$Comp
L Device:R R5
U 1 1 5F7AEA01
P 3800 1550
F 0 "R5" H 3870 1596 50  0000 L CNN
F 1 "10k" H 3870 1505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3730 1550 50  0001 C CNN
F 3 "~" H 3800 1550 50  0001 C CNN
	1    3800 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1850 3800 1700
Connection ~ 3800 1850
Wire Wire Line
	3800 1850 3950 1850
Wire Wire Line
	3800 1400 3800 1300
$Comp
L power:VCC #PWR0106
U 1 1 5F7AF919
P 3800 1300
F 0 "#PWR0106" H 3800 1150 50  0001 C CNN
F 1 "VCC" H 3815 1473 50  0000 C CNN
F 2 "" H 3800 1300 50  0001 C CNN
F 3 "" H 3800 1300 50  0001 C CNN
	1    3800 1300
	1    0    0    -1  
$EndComp
Text GLabel 3950 1850 2    50   Output ~ 0
ZC_DETECTION
Text GLabel 2750 3350 2    50   UnSpc ~ 0
AC_PWR
Text Notes 1250 850  0    89   ~ 0
ZERO CROSSING DETECTION
Wire Notes Line
	1200 2650 1200 700 
$Comp
L Triac_Thyristor:BTA16-600B Q1
U 1 1 5F7BEC2F
P 6000 2300
F 0 "Q1" H 6129 2346 50  0000 L CNN
F 1 "BTA41-600E 40A TO3P" H 6129 2255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-3P-3_Vertical" H 6200 2225 50  0001 L CIN
F 3 "https://www.st.com/resource/en/datasheet/bta16.pdf" H 6000 2300 50  0001 L CNN
	1    6000 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5F7C18C5
P 6350 1950
F 0 "R6" V 6143 1950 50  0000 C CNN
F 1 "220R/300V" V 6234 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_2010_5025Metric_Pad1.52x2.65mm_HandSolder" V 6280 1950 50  0001 C CNN
F 3 "~" H 6350 1950 50  0001 C CNN
	1    6350 1950
	0    1    1    0   
$EndComp
$Comp
L Relay_SolidState:MOC3021M U2
U 1 1 5F7C2654
P 7050 1800
F 0 "U2" H 7050 1483 50  0000 C CNN
F 1 "MOC3021M" H 7050 1574 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W8.89mm_SMDSocket_LongPads" H 6850 1600 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/MOC3023M-D.PDF" H 7050 1800 50  0001 L CNN
	1    7050 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 1950 6750 1950
Wire Wire Line
	6000 1950 6000 2150
Wire Wire Line
	2300 2050 2050 2050
Wire Wire Line
	2050 2050 2050 2200
$Comp
L power:GND1 #PWR0107
U 1 1 5F7E596D
P 2050 2200
F 0 "#PWR0107" H 2050 1950 50  0001 C CNN
F 1 "GND1" H 2055 2027 50  0000 C CNN
F 2 "" H 2050 2200 50  0001 C CNN
F 3 "" H 2050 2200 50  0001 C CNN
	1    2050 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1750 1850 1750
Connection ~ 2050 1750
Text GLabel 1850 1750 0    50   UnSpc ~ 0
AC_PWR
Wire Notes Line
	1200 2650 4700 2650
Wire Notes Line
	4700 2650 4700 700 
Wire Notes Line
	4700 700  1200 700 
$Comp
L Device:Fuse F1
U 1 1 5F803423
P 2350 3350
F 0 "F1" V 2153 3350 50  0000 C CNN
F 1 "Fuse" V 2244 3350 50  0000 C CNN
F 2 "Fuse:Fuseholder_Cylinder-5x20mm_Schurter_OGN-SMD_Horizontal_Open" V 2280 3350 50  0001 C CNN
F 3 "~" H 2350 3350 50  0001 C CNN
	1    2350 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 2450 6000 2650
Wire Wire Line
	6000 2650 5850 2650
Text GLabel 5850 2650 0    50   Output ~ 0
AC_CONTROLLED_OUT
Text Notes 1250 3050 0    89   ~ 0
TERMINALS\n
Wire Wire Line
	6200 1950 6000 1950
Wire Wire Line
	5850 2400 5700 2400
Wire Wire Line
	6750 1950 6750 1900
Wire Wire Line
	6750 1700 5700 1700
Wire Wire Line
	5700 1700 5700 2400
Connection ~ 6000 1950
Text GLabel 6050 1350 2    50   UnSpc ~ 0
AC_PWR
Wire Wire Line
	6000 1350 6050 1350
Wire Wire Line
	6000 1350 6000 1950
$Comp
L Device:R R9
U 1 1 5F842CDC
P 7750 2000
F 0 "R9" V 7543 2000 50  0000 C CNN
F 1 "100R" V 7634 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7680 2000 50  0001 C CNN
F 3 "~" H 7750 2000 50  0001 C CNN
	1    7750 2000
	0    1    1    0   
$EndComp
Wire Wire Line
	7350 1900 7500 1900
$Comp
L power:GND #PWR0108
U 1 1 5F8485EF
P 8100 1750
F 0 "#PWR0108" H 8100 1500 50  0001 C CNN
F 1 "GND" H 8105 1577 50  0000 C CNN
F 2 "" H 8100 1750 50  0001 C CNN
F 3 "" H 8100 1750 50  0001 C CNN
	1    8100 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1900 7500 2000
Wire Wire Line
	8100 1700 8100 1750
Wire Wire Line
	7350 1700 8100 1700
Wire Wire Line
	7500 2000 7600 2000
Wire Wire Line
	7900 2000 8200 2000
Text GLabel 8200 2000 2    50   Input ~ 0
TRIAC_CONTROL
Text Notes 4950 850  0    89   ~ 0
AC CONTROL \n
Wire Notes Line
	4900 700  4900 2950
Wire Notes Line
	4900 2950 8900 2950
Wire Notes Line
	8900 2950 8900 700 
Wire Notes Line
	8900 700  4900 700 
$Comp
L Device:Varistor RV1
U 1 1 5F880950
P 2600 3600
F 0 "RV1" H 2703 3646 50  0000 L CNN
F 1 " V250LA10P" H 2703 3555 50  0000 L CNN
F 2 "Varistor:RV_Disc_D12mm_W3.9mm_P7.5mm" V 2530 3600 50  0001 C CNN
F 3 "~" H 2600 3600 50  0001 C CNN
	1    2600 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3350 2600 3350
Wire Wire Line
	2600 3350 2600 3450
Wire Wire Line
	2600 3350 2750 3350
Connection ~ 2600 3350
Wire Wire Line
	1800 3700 2200 3700
Wire Wire Line
	2200 3700 2200 3350
Wire Wire Line
	2200 3800 2600 3800
Wire Wire Line
	2600 3800 2600 3750
Connection ~ 2200 3800
$Comp
L Converter_ACDC:HLK-PM01 PS1
U 1 1 5F8BECC2
P 7250 4150
F 0 "PS1" H 7250 4475 50  0000 C CNN
F 1 "HLK-PM01(5V)" H 7250 4384 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_HiLink_HLK-PMxx" H 7250 3850 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=54" H 7650 3800 50  0001 C CNN
	1    7250 4150
	1    0    0    -1  
$EndComp
Text GLabel 5300 4000 0    50   UnSpc ~ 0
AC_PWR
$Comp
L power:GND1 #PWR0109
U 1 1 5F8C2FA9
P 5250 4400
F 0 "#PWR0109" H 5250 4150 50  0001 C CNN
F 1 "GND1" H 5255 4227 50  0000 C CNN
F 2 "" H 5250 4400 50  0001 C CNN
F 3 "" H 5250 4400 50  0001 C CNN
	1    5250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4300 5250 4400
$Comp
L power:VCC #PWR0110
U 1 1 5F8C85C5
P 10650 3550
F 0 "#PWR0110" H 10650 3400 50  0001 C CNN
F 1 "VCC" H 10665 3723 50  0000 C CNN
F 2 "" H 10650 3550 50  0001 C CNN
F 3 "" H 10650 3550 50  0001 C CNN
	1    10650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4050 7800 4050
Wire Wire Line
	7800 4050 7800 3950
$Comp
L Device:CP C5
U 1 1 5F8CAE47
P 8650 4150
F 0 "C5" H 8768 4196 50  0000 L CNN
F 1 "220uF Tantalum" H 8768 4105 50  0000 L CNN
F 2 "Capacitor_SMD:C_2816_7142Metric_Pad3.20x4.45mm_HandSolder" H 8688 4000 50  0001 C CNN
F 3 "293D227X96R3C2TE3 VISHAY tme" H 8650 4150 50  0001 C CNN
	1    8650 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F8D254B
P 2450 1350
F 0 "C1" V 2198 1350 50  0000 C CNN
F 1 "47n/400V" V 2289 1350 50  0000 C CNN
F 2 "Capacitor_THT:C_Rect_L16.5mm_W7.0mm_P15.00mm_MKT" H 2488 1200 50  0001 C CNN
F 3 "~" H 2450 1350 50  0001 C CNN
	1    2450 1350
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 1350 2300 1350
Wire Wire Line
	2600 1350 2900 1350
Wire Wire Line
	7800 3950 8150 3950
Wire Wire Line
	8150 3950 8150 4000
Wire Wire Line
	8650 3950 8650 3900
Wire Wire Line
	7650 4250 7800 4250
Wire Wire Line
	7800 4250 7800 4400
Wire Wire Line
	7800 4400 8150 4400
Wire Wire Line
	8150 4400 8150 4300
$Comp
L power:GND #PWR0111
U 1 1 5F8E6474
P 8650 4500
F 0 "#PWR0111" H 8650 4250 50  0001 C CNN
F 1 "GND" H 8655 4327 50  0000 C CNN
F 2 "" H 8650 4500 50  0001 C CNN
F 3 "" H 8650 4500 50  0001 C CNN
	1    8650 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 4400 8650 4500
$Comp
L Device:C C4
U 1 1 5F8EDB5E
P 8150 4150
F 0 "C4" H 8035 4104 50  0000 R CNN
F 1 "100n" H 8035 4195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8188 4000 50  0001 C CNN
F 3 "~" H 8150 4150 50  0001 C CNN
	1    8150 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 3950 8650 3950
Wire Wire Line
	8650 3950 8650 4000
Connection ~ 8150 3950
Wire Wire Line
	8150 4400 8650 4400
Wire Wire Line
	8650 4400 8650 4300
Connection ~ 8150 4400
Connection ~ 8650 3950
Connection ~ 8650 4400
$Comp
L Device:L_Core_Ferrite_Coupled_1243 L1
U 1 1 5F8FC846
P 6450 4150
F 0 "L1" H 6450 4431 50  0000 C CNN
F 1 "10mH choke" H 6450 4340 50  0000 C CNN
F 2 "Inductor_THT:L_CommonMode_Wuerth_WE-CMB-XS" H 6450 4150 50  0001 C CNN
F 3 "~" H 6450 4150 50  0001 C CNN
	1    6450 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 4050 6850 4050
Wire Wire Line
	6650 4250 6850 4250
$Comp
L Device:C C2
U 1 1 5F9119A4
P 5700 4150
F 0 "C2" H 5585 4104 50  0000 R CNN
F 1 "100n/250V" H 5585 4195 50  0000 R CNN
F 2 "Capacitor_THT:C_Rect_L16.5mm_W7.0mm_P15.00mm_MKT" H 5738 4000 50  0001 C CNN
F 3 "~" H 5700 4150 50  0001 C CNN
	1    5700 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	6250 4050 6150 4050
Wire Wire Line
	6150 4050 6150 4000
Wire Wire Line
	6250 4250 6150 4250
Wire Wire Line
	6150 4250 6150 4300
Wire Wire Line
	5700 4300 6150 4300
Wire Wire Line
	5700 4000 6150 4000
Connection ~ 5700 4000
Wire Wire Line
	5300 4000 5700 4000
Connection ~ 5700 4300
Wire Wire Line
	5250 4300 5700 4300
Text Notes 4950 3300 0    89   ~ 0
LOGIC POWER SUPPLY
Wire Notes Line
	4900 3150 4900 4750
Wire Notes Line
	11150 4750 11150 3150
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5F81AC59
P 1600 5750
F 0 "J3" H 1518 5425 50  0000 C CNN
F 1 "CONTROL_SIGNAL" H 1518 5516 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00067_1x02_P7.50mm_Horizontal" H 1600 5750 50  0001 C CNN
F 3 "~" H 1600 5750 50  0001 C CNN
	1    1600 5750
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5F952568
P 2150 5800
F 0 "#PWR0112" H 2150 5550 50  0001 C CNN
F 1 "GND" H 2155 5627 50  0000 C CNN
F 2 "" H 2150 5800 50  0001 C CNN
F 3 "" H 2150 5800 50  0001 C CNN
	1    2150 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5750 2150 5800
Wire Wire Line
	1800 5750 2150 5750
Wire Wire Line
	1800 5650 1900 5650
Text GLabel 1900 5650 2    50   Output ~ 0
CTRL_SIGNAL
$Comp
L Device:D_Schottky D1
U 1 1 5F9670BE
P 5750 5450
F 0 "D1" H 5750 5233 50  0000 C CNN
F 1 "D_Schottky" H 5750 5324 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5750 5450 50  0001 C CNN
F 3 " TSS40U-RGG" H 5750 5450 50  0001 C CNN
	1    5750 5450
	-1   0    0    1   
$EndComp
$Comp
L Device:Fuse F2
U 1 1 5F967C3F
P 6200 5450
F 0 "F2" V 6003 5450 50  0000 C CNN
F 1 "Fuse" V 6094 5450 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6130 5450 50  0001 C CNN
F 3 "FUSE 0603-FF PW 0A5" H 6200 5450 50  0001 C CNN
	1    6200 5450
	0    1    1    0   
$EndComp
$Comp
L Diode:SM6T12A D2
U 1 1 5F969CB8
P 6500 5700
F 0 "D2" V 6454 5780 50  0000 L CNN
F 1 "SM6T12A" V 6545 5780 50  0000 L CNN
F 2 "Diode_SMD:D_SMB" H 6500 5500 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/sm6t.pdf" H 6450 5700 50  0001 C CNN
	1    6500 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 5450 6000 5450
Wire Wire Line
	6350 5450 6500 5450
Wire Wire Line
	6500 5450 6500 5550
$Comp
L Device:R R7
U 1 1 5F97126C
P 6750 5450
F 0 "R7" V 6543 5450 50  0000 C CNN
F 1 "10K" V 6634 5450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6680 5450 50  0001 C CNN
F 3 "~" H 6750 5450 50  0001 C CNN
	1    6750 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 5450 6600 5450
Connection ~ 6500 5450
$Comp
L Device:R R8
U 1 1 5F97647A
P 7050 5700
F 0 "R8" H 6980 5654 50  0000 R CNN
F 1 "3,6K" H 6980 5745 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6980 5700 50  0001 C CNN
F 3 "~" H 7050 5700 50  0001 C CNN
	1    7050 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 5450 7050 5450
Wire Wire Line
	7050 5450 7050 5550
Wire Wire Line
	7050 5450 7200 5450
Connection ~ 7050 5450
Wire Wire Line
	7050 5850 7050 5950
$Comp
L power:GND #PWR0113
U 1 1 5F97B780
P 7050 6000
F 0 "#PWR0113" H 7050 5750 50  0001 C CNN
F 1 "GND" H 7055 5827 50  0000 C CNN
F 2 "" H 7050 6000 50  0001 C CNN
F 3 "" H 7050 6000 50  0001 C CNN
	1    7050 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5950 7050 5950
Connection ~ 7050 5950
Wire Wire Line
	7050 5950 7050 6000
Text GLabel 5500 5450 0    50   Input ~ 0
CTRL_SIGNAL
Wire Wire Line
	5500 5450 5600 5450
Text GLabel 7200 5450 2    50   Output ~ 0
CTRL_SIG_ADC
$Comp
L Device:C C3
U 1 1 5F99014B
P 6000 5700
F 0 "C3" H 5885 5654 50  0000 R CNN
F 1 "100n" H 5885 5745 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6038 5550 50  0001 C CNN
F 3 "~" H 6000 5700 50  0001 C CNN
	1    6000 5700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 5450 6000 5550
Connection ~ 6000 5450
Wire Wire Line
	6000 5450 6050 5450
Wire Wire Line
	6500 5850 6500 5950
Wire Wire Line
	6000 5850 6000 5950
Wire Wire Line
	6000 5950 6500 5950
Connection ~ 6500 5950
Text Notes 4950 5050 0    89   ~ 0
CONTROL SIGNAL PREPARATION\n
Wire Notes Line
	4900 4900 4900 6250
Wire Notes Line
	4900 6250 7950 6250
Wire Notes Line
	7950 6250 7950 4900
Wire Notes Line
	7950 4900 4900 4900
$Sheet
S 9450 750  1350 1500
U 5F7C1723
F0 "uc_and_adjustment" 50
F1 "uc_and_adjustment.sch" 50
$EndSheet
$Comp
L Device:LED D3
U 1 1 5F7D6985
P 7750 3550
F 0 "D3" H 7743 3295 50  0000 C CNN
F 1 "PWR_LED_GREEN" H 7743 3386 50  0000 C CNN
F 2 "Diode_SMD:D_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7750 3550 50  0001 C CNN
F 3 "RF-GNB191TS-CF" H 7750 3550 50  0001 C CNN
	1    7750 3550
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0114
U 1 1 5F7E393C
P 6900 3500
F 0 "#PWR0114" H 6900 3350 50  0001 C CNN
F 1 "VCC" H 6915 3673 50  0000 C CNN
F 2 "" H 6900 3500 50  0001 C CNN
F 3 "" H 6900 3500 50  0001 C CNN
	1    6900 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5F7E4F5B
P 7300 3550
F 0 "R10" V 7093 3550 50  0000 C CNN
F 1 "220R" V 7184 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7230 3550 50  0001 C CNN
F 3 "~" H 7300 3550 50  0001 C CNN
	1    7300 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 3500 6900 3550
Wire Wire Line
	6900 3550 7150 3550
Wire Wire Line
	7450 3550 7600 3550
$Comp
L power:GND #PWR0115
U 1 1 5F7EB879
P 8250 3600
F 0 "#PWR0115" H 8250 3350 50  0001 C CNN
F 1 "GND" H 8255 3427 50  0000 C CNN
F 2 "" H 8250 3600 50  0001 C CNN
F 3 "" H 8250 3600 50  0001 C CNN
	1    8250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 3550 8250 3550
Wire Wire Line
	8250 3550 8250 3600
$Comp
L power:VCC #PWR0116
U 1 1 5F9530EC
P 2150 6300
F 0 "#PWR0116" H 2150 6150 50  0001 C CNN
F 1 "VCC" H 2165 6473 50  0000 C CNN
F 2 "" H 2150 6300 50  0001 C CNN
F 3 "" H 2150 6300 50  0001 C CNN
	1    2150 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6350 2150 6350
Wire Wire Line
	2150 6350 2150 6300
Text GLabel 3400 5850 2    50   Output ~ 0
TEMP_ADC
Wire Wire Line
	3100 6450 3100 6550
Wire Wire Line
	2850 6450 3100 6450
$Comp
L power:GND #PWR0117
U 1 1 5F94E40B
P 3100 6550
F 0 "#PWR0117" H 3100 6300 50  0001 C CNN
F 1 "GND" H 3105 6377 50  0000 C CNN
F 2 "" H 3100 6550 50  0001 C CNN
F 3 "" H 3100 6550 50  0001 C CNN
	1    3100 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 6450 2550 6450
Connection ~ 2450 6450
Wire Wire Line
	1800 6450 2450 6450
$Comp
L Device:R R4
U 1 1 5F947344
P 2700 6450
F 0 "R4" V 2493 6450 50  0000 C CNN
F 1 "10K" V 2584 6450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2630 6450 50  0001 C CNN
F 3 "~" H 2700 6450 50  0001 C CNN
	1    2700 6450
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J4
U 1 1 5F81CB41
P 1600 6450
F 0 "J4" H 1518 6125 50  0000 C CNN
F 1 "TEMP_SENSOR" H 1518 6216 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00067_1x02_P7.50mm_Horizontal" H 1600 6450 50  0001 C CNN
F 3 "~" H 1600 6450 50  0001 C CNN
	1    1600 6450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5F98DCD9
P 2350 6950
F 0 "R1" V 2143 6950 50  0000 C CNN
F 1 "10K" V 2234 6950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2280 6950 50  0001 C CNN
F 3 "~" H 2350 6950 50  0001 C CNN
	1    2350 6950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5F98DCCF
P 2150 7150
F 0 "#PWR0118" H 2150 6900 50  0001 C CNN
F 1 "GND" H 2155 6977 50  0000 C CNN
F 2 "" H 2150 7150 50  0001 C CNN
F 3 "" H 2150 7150 50  0001 C CNN
	1    2150 7150
	1    0    0    -1  
$EndComp
Text GLabel 2650 6950 2    50   Output ~ 0
PRESSURE_ADC
Wire Wire Line
	2150 6850 2150 6800
Wire Wire Line
	1800 6850 2150 6850
$Comp
L Connector:Screw_Terminal_01x03 J5
U 1 1 5F9FC5F7
P 1600 6950
F 0 "J5" H 1518 6625 50  0000 C CNN
F 1 "PRESS_SENSOR" H 1518 6716 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00068_1x03_P7.50mm_Horizontal" H 1600 6950 50  0001 C CNN
F 3 "~" H 1600 6950 50  0001 C CNN
	1    1600 6950
	-1   0    0    1   
$EndComp
Wire Wire Line
	2150 7050 2150 7150
Wire Wire Line
	1800 7050 2150 7050
Wire Wire Line
	2500 6950 2550 6950
Wire Wire Line
	1800 6950 2200 6950
$Comp
L Connector:Screw_Terminal_01x03 J6
U 1 1 5FA37EA4
P 3800 3600
F 0 "J6" H 3718 3275 50  0000 C CNN
F 1 "RS485" H 3718 3366 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00068_1x03_P7.50mm_Horizontal" H 3800 3600 50  0001 C CNN
F 3 "~" H 3800 3600 50  0001 C CNN
	1    3800 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 3500 4150 3500
Wire Wire Line
	4000 3600 4150 3600
Wire Wire Line
	4000 3700 4150 3700
Text GLabel 4150 3500 2    50   Input ~ 0
A
Text GLabel 4150 3600 2    50   Input ~ 0
B
Wire Wire Line
	4150 3700 4150 3750
$Comp
L power:GND #PWR0120
U 1 1 5FA50811
P 4150 3750
F 0 "#PWR0120" H 4150 3500 50  0001 C CNN
F 1 "GND" H 4155 3577 50  0000 C CNN
F 2 "" H 4150 3750 50  0001 C CNN
F 3 "" H 4150 3750 50  0001 C CNN
	1    4150 3750
	1    0    0    -1  
$EndComp
Wire Notes Line
	1200 2900 4500 2900
Wire Notes Line
	1200 7700 4500 7700
NoConn ~ -100 4950
$Comp
L Device:R R23
U 1 1 5FAD49D7
P 2550 7200
F 0 "R23" V 2343 7200 50  0000 C CNN
F 1 "16K" V 2434 7200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2480 7200 50  0001 C CNN
F 3 "~" H 2550 7200 50  0001 C CNN
	1    2550 7200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2550 6950 2550 7050
Connection ~ 2550 6950
Wire Wire Line
	2550 6950 2650 6950
$Comp
L power:GND #PWR0146
U 1 1 5FAE2C9C
P 2550 7450
F 0 "#PWR0146" H 2550 7200 50  0001 C CNN
F 1 "GND" H 2555 7277 50  0000 C CNN
F 2 "" H 2550 7450 50  0001 C CNN
F 3 "" H 2550 7450 50  0001 C CNN
	1    2550 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 7350 2550 7450
Wire Notes Line
	1200 2900 1200 7700
Wire Notes Line
	4500 2900 4500 7700
$Comp
L Device:C C6
U 1 1 5FAA41B7
P 3100 6200
F 0 "C6" H 2985 6154 50  0000 R CNN
F 1 "100n" H 2985 6245 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3138 6050 50  0001 C CNN
F 3 "~" H 3100 6200 50  0001 C CNN
	1    3100 6200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 6350 3100 6450
Connection ~ 3100 6450
Wire Wire Line
	2450 5850 3100 5850
Wire Wire Line
	2450 5850 2450 6450
Wire Wire Line
	3100 5850 3100 6050
Connection ~ 3100 5850
Wire Wire Line
	3100 5850 3400 5850
Wire Wire Line
	1800 4800 2050 4800
Wire Wire Line
	1800 4700 2250 4700
Wire Wire Line
	2250 4700 2250 5000
Wire Wire Line
	1800 4900 2450 4900
Text GLabel 2450 4900 2    50   Input ~ 0
AC_CONTROLLED_OUT
$Comp
L power:VCCQ #PWR0119
U 1 1 5FAE7DE4
P 8650 3900
F 0 "#PWR0119" H 8650 3750 50  0001 C CNN
F 1 "VCCQ" H 8665 4073 50  0000 C CNN
F 2 "" H 8650 3900 50  0001 C CNN
F 3 "" H 8650 3900 50  0001 C CNN
	1    8650 3900
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AP1117-18 U5
U 1 1 5FAF33AD
P 9800 3650
F 0 "U5" H 9800 3892 50  0000 C CNN
F 1 "LD1117S33CTR" H 9800 3801 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 9800 3850 50  0001 C CNN
F 3 "https://pl.mouser.com/datasheet/2/389/ld1117-1849389.pdf" H 9900 3400 50  0001 C CNN
	1    9800 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 3650 10200 3650
Wire Wire Line
	9500 3650 9300 3650
Wire Wire Line
	9300 3650 9300 3550
$Comp
L power:VCCQ #PWR0165
U 1 1 5FB0F504
P 9300 3550
F 0 "#PWR0165" H 9300 3400 50  0001 C CNN
F 1 "VCCQ" H 9315 3723 50  0000 C CNN
F 2 "" H 9300 3550 50  0001 C CNN
F 3 "" H 9300 3550 50  0001 C CNN
	1    9300 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0166
U 1 1 5FB15D4F
P 9800 4250
F 0 "#PWR0166" H 9800 4000 50  0001 C CNN
F 1 "GND" H 9805 4077 50  0000 C CNN
F 2 "" H 9800 4250 50  0001 C CNN
F 3 "" H 9800 4250 50  0001 C CNN
	1    9800 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5FB2B2F3
P 10200 3900
F 0 "C16" H 10085 3854 50  0000 R CNN
F 1 "100n" H 10085 3945 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10238 3750 50  0001 C CNN
F 3 "~" H 10200 3900 50  0001 C CNN
	1    10200 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	10200 3650 10200 3750
Connection ~ 10200 3650
Wire Wire Line
	9800 3950 9800 4100
Wire Wire Line
	10200 4050 10200 4100
Wire Wire Line
	10200 4100 9800 4100
Connection ~ 9800 4100
Wire Wire Line
	9800 4100 9800 4250
Wire Wire Line
	10200 3650 10650 3650
$Comp
L Device:C C17
U 1 1 5FB4F394
P 10650 3900
F 0 "C17" H 10535 3854 50  0000 R CNN
F 1 "2,2uF" H 10535 3945 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10688 3750 50  0001 C CNN
F 3 "~" H 10650 3900 50  0001 C CNN
	1    10650 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	10650 3650 10650 3750
Wire Wire Line
	10200 4100 10650 4100
Wire Wire Line
	10650 4100 10650 4050
Connection ~ 10200 4100
Wire Wire Line
	10650 3650 10650 3550
Connection ~ 10650 3650
Wire Notes Line
	4900 3150 11150 3150
Wire Notes Line
	4900 4750 11150 4750
$Comp
L power:VCCQ #PWR0167
U 1 1 5FBB90B7
P 2150 6800
F 0 "#PWR0167" H 2150 6650 50  0001 C CNN
F 1 "VCCQ" H 2165 6973 50  0000 C CNN
F 2 "" H 2150 6800 50  0001 C CNN
F 3 "" H 2150 6800 50  0001 C CNN
	1    2150 6800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
