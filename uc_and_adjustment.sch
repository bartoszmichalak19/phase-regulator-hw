EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_POT RV2
U 1 1 5F7D3D7E
P 7600 1450
F 0 "RV2" V 7393 1450 50  0000 C CNN
F 1 "R_POT" V 7484 1450 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Runtron_RM-065_Vertical" H 7600 1450 50  0001 C CNN
F 3 "RKT6V-10K" H 7600 1450 50  0001 C CNN
	1    7600 1450
	0    1    1    0   
$EndComp
$Comp
L Device:LED D4
U 1 1 5F7D437C
P 10400 5150
F 0 "D4" H 10393 4895 50  0000 C CNN
F 1 "LED_RED" H 10393 4986 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10400 5150 50  0001 C CNN
F 3 "RF-RUB190TS-CA" H 10400 5150 50  0001 C CNN
	1    10400 5150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R22
U 1 1 5F7E4FA0
P 10000 5150
F 0 "R22" V 9793 5150 50  0000 C CNN
F 1 "220R" V 9884 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9930 5150 50  0001 C CNN
F 3 "~" H 10000 5150 50  0001 C CNN
	1    10000 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 5150 10250 5150
Wire Wire Line
	10550 5150 10850 5150
Wire Wire Line
	9850 5150 9700 5150
Text GLabel 9700 5150 0    50   Input ~ 0
LED_R
$Comp
L Device:LED D5
U 1 1 5F7F183C
P 10400 5650
F 0 "D5" H 10393 5395 50  0000 C CNN
F 1 "LED_GREEN" H 10393 5486 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10400 5650 50  0001 C CNN
F 3 "RF-GNB191TS-CF" H 10400 5650 50  0001 C CNN
	1    10400 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	10150 5650 10250 5650
Wire Wire Line
	10550 5650 10850 5650
Wire Wire Line
	9850 5650 9700 5650
Text GLabel 9700 5650 0    50   Input ~ 0
LED_G
$Comp
L Device:LED D6
U 1 1 5F7F2323
P 10400 6150
F 0 "D6" H 10393 5895 50  0000 C CNN
F 1 "LED_YELLOW" H 10393 5986 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10400 6150 50  0001 C CNN
F 3 "RF-YUB190TS-CA " H 10400 6150 50  0001 C CNN
	1    10400 6150
	-1   0    0    1   
$EndComp
Wire Wire Line
	10150 6150 10250 6150
$Comp
L power:GND #PWR0121
U 1 1 5F7F2330
P 10850 6250
F 0 "#PWR0121" H 10850 6000 50  0001 C CNN
F 1 "GND" H 10855 6077 50  0000 C CNN
F 2 "" H 10850 6250 50  0001 C CNN
F 3 "" H 10850 6250 50  0001 C CNN
	1    10850 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 6150 10850 6250
Wire Wire Line
	10550 6150 10850 6150
Wire Wire Line
	9850 6150 9700 6150
Text GLabel 9700 6150 0    50   Input ~ 0
LED_Y
Wire Wire Line
	10850 5150 10850 5650
Connection ~ 10850 5650
Wire Wire Line
	10850 5650 10850 6150
Connection ~ 10850 6150
$Comp
L Jumper:SolderJumper_2_Open JP2
U 1 1 5FA5C31B
P 9950 1200
F 0 "JP2" H 9950 1405 50  0000 C CNN
F 1 "SolderJumper_TEMP_OR_PRESS" H 9950 1314 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9950 1200 50  0001 C CNN
F 3 "~" H 9950 1200 50  0001 C CNN
	1    9950 1200
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP3
U 1 1 5FA5F7B0
P 9950 1950
F 0 "JP3" H 9950 2155 50  0000 C CNN
F 1 "SolderJumper_TRIM_OR_CTRL_SIG" H 9950 2064 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9950 1950 50  0001 C CNN
F 3 "~" H 9950 1950 50  0001 C CNN
	1    9950 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5FA65544
P 9100 1950
F 0 "R18" V 8893 1950 50  0000 C CNN
F 1 "10k" V 8984 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9030 1950 50  0001 C CNN
F 3 "~" H 9100 1950 50  0001 C CNN
	1    9100 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 1950 9300 1950
$Comp
L Device:R R17
U 1 1 5FA680C2
P 9100 1200
F 0 "R17" V 8893 1200 50  0000 C CNN
F 1 "10k" V 8984 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9030 1200 50  0001 C CNN
F 3 "~" H 9100 1200 50  0001 C CNN
	1    9100 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 1200 9300 1200
Wire Wire Line
	9300 1950 9300 1650
Wire Wire Line
	9300 1650 8950 1650
Connection ~ 9300 1950
Wire Wire Line
	9300 1950 9800 1950
Wire Wire Line
	9300 1200 9300 900 
Wire Wire Line
	9300 900  8950 900 
Connection ~ 9300 1200
Wire Wire Line
	9300 1200 9800 1200
$Comp
L power:GND #PWR0122
U 1 1 5FA6AB22
P 8700 2050
F 0 "#PWR0122" H 8700 1800 50  0001 C CNN
F 1 "GND" H 8705 1877 50  0000 C CNN
F 2 "" H 8700 2050 50  0001 C CNN
F 3 "" H 8700 2050 50  0001 C CNN
	1    8700 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1950 8700 2050
Wire Wire Line
	8700 1950 8950 1950
$Comp
L power:GND #PWR0123
U 1 1 5FA6C1AF
P 8700 1300
F 0 "#PWR0123" H 8700 1050 50  0001 C CNN
F 1 "GND" H 8705 1127 50  0000 C CNN
F 2 "" H 8700 1300 50  0001 C CNN
F 3 "" H 8700 1300 50  0001 C CNN
	1    8700 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 1200 8700 1300
Wire Wire Line
	8700 1200 8950 1200
Text GLabel 8950 900  0    50   Output ~ 0
TEMP_OR_PRESS
Text GLabel 8950 1650 0    50   Output ~ 0
TRIM_OR_CTRLSIG
Wire Wire Line
	10100 1200 10650 1200
Wire Wire Line
	10650 1200 10650 1050
Wire Wire Line
	10100 1950 10650 1950
Wire Wire Line
	10650 1950 10650 1850
$Comp
L Jumper:SolderJumper_2_Open JP4
U 1 1 5F96BBD8
P 9950 2750
F 0 "JP4" H 9950 2955 50  0000 C CNN
F 1 "SolderJumper_AUTO_OR_EXTERNAL" H 9950 2864 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9950 2750 50  0001 C CNN
F 3 "~" H 9950 2750 50  0001 C CNN
	1    9950 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5F96BBDE
P 9100 2750
F 0 "R19" V 8893 2750 50  0000 C CNN
F 1 "10k" V 8984 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9030 2750 50  0001 C CNN
F 3 "~" H 9100 2750 50  0001 C CNN
	1    9100 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 2750 9300 2750
Wire Wire Line
	9300 2750 9300 2450
Wire Wire Line
	9300 2450 8950 2450
Connection ~ 9300 2750
Wire Wire Line
	9300 2750 9800 2750
$Comp
L power:GND #PWR0124
U 1 1 5F96BBE9
P 8700 2850
F 0 "#PWR0124" H 8700 2600 50  0001 C CNN
F 1 "GND" H 8705 2677 50  0000 C CNN
F 2 "" H 8700 2850 50  0001 C CNN
F 3 "" H 8700 2850 50  0001 C CNN
	1    8700 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 2750 8700 2850
Wire Wire Line
	8700 2750 8950 2750
Text GLabel 8950 2450 0    50   Output ~ 0
AUTO_OR_EXTERNAL
Wire Wire Line
	10100 2750 10650 2750
Wire Wire Line
	10650 2750 10650 2650
$Comp
L power:VCC #PWR0125
U 1 1 5F9710F3
P 10650 2650
F 0 "#PWR0125" H 10650 2500 50  0001 C CNN
F 1 "VCC" H 10665 2823 50  0000 C CNN
F 2 "" H 10650 2650 50  0001 C CNN
F 3 "" H 10650 2650 50  0001 C CNN
	1    10650 2650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0126
U 1 1 5F971960
P 10650 1850
F 0 "#PWR0126" H 10650 1700 50  0001 C CNN
F 1 "VCC" H 10665 2023 50  0000 C CNN
F 2 "" H 10650 1850 50  0001 C CNN
F 3 "" H 10650 1850 50  0001 C CNN
	1    10650 1850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0127
U 1 1 5F97276F
P 10650 1050
F 0 "#PWR0127" H 10650 900 50  0001 C CNN
F 1 "VCC" H 10665 1223 50  0000 C CNN
F 2 "" H 10650 1050 50  0001 C CNN
F 3 "" H 10650 1050 50  0001 C CNN
	1    10650 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0128
U 1 1 5F97F5AC
P 7250 1500
F 0 "#PWR0128" H 7250 1250 50  0001 C CNN
F 1 "GND" H 7255 1327 50  0000 C CNN
F 2 "" H 7250 1500 50  0001 C CNN
F 3 "" H 7250 1500 50  0001 C CNN
	1    7250 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1450 7250 1450
Wire Wire Line
	7250 1450 7250 1500
$Comp
L power:VCC #PWR0129
U 1 1 5F9807FB
P 7950 1400
F 0 "#PWR0129" H 7950 1250 50  0001 C CNN
F 1 "VCC" H 7965 1573 50  0000 C CNN
F 2 "" H 7950 1400 50  0001 C CNN
F 3 "" H 7950 1400 50  0001 C CNN
	1    7950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 1450 7950 1450
Wire Wire Line
	7950 1450 7950 1400
Text GLabel 7450 1850 0    50   Input ~ 0
TRIM1
Wire Wire Line
	7600 1850 7450 1850
Wire Wire Line
	7600 1600 7600 1850
$Comp
L power:GND #PWR0130
U 1 1 5F997DD3
P 7250 2250
F 0 "#PWR0130" H 7250 2000 50  0001 C CNN
F 1 "GND" H 7255 2077 50  0000 C CNN
F 2 "" H 7250 2250 50  0001 C CNN
F 3 "" H 7250 2250 50  0001 C CNN
	1    7250 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2200 7250 2200
Wire Wire Line
	7250 2200 7250 2250
$Comp
L power:VCC #PWR0131
U 1 1 5F997DDB
P 7950 2150
F 0 "#PWR0131" H 7950 2000 50  0001 C CNN
F 1 "VCC" H 7965 2323 50  0000 C CNN
F 2 "" H 7950 2150 50  0001 C CNN
F 3 "" H 7950 2150 50  0001 C CNN
	1    7950 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2200 7950 2200
Wire Wire Line
	7950 2200 7950 2150
Text GLabel 7450 2600 0    50   Input ~ 0
TRIM2
Wire Wire Line
	7600 2600 7450 2600
Wire Wire Line
	7600 2350 7600 2600
$Comp
L Jumper:SolderJumper_2_Open JP5
U 1 1 5FADCFE6
P 9950 3500
F 0 "JP5" H 9950 3705 50  0000 C CNN
F 1 "SolderJumper_RS485" H 9950 3614 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9950 3500 50  0001 C CNN
F 3 "~" H 9950 3500 50  0001 C CNN
	1    9950 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5FADCFEC
P 9100 3500
F 0 "R20" V 8893 3500 50  0000 C CNN
F 1 "10k" V 8984 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9030 3500 50  0001 C CNN
F 3 "~" H 9100 3500 50  0001 C CNN
	1    9100 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 3500 9300 3500
Wire Wire Line
	9300 3500 9300 3200
Wire Wire Line
	9300 3200 8950 3200
Connection ~ 9300 3500
Wire Wire Line
	9300 3500 9800 3500
$Comp
L power:GND #PWR0136
U 1 1 5FADCFF7
P 8700 3600
F 0 "#PWR0136" H 8700 3350 50  0001 C CNN
F 1 "GND" H 8705 3427 50  0000 C CNN
F 2 "" H 8700 3600 50  0001 C CNN
F 3 "" H 8700 3600 50  0001 C CNN
	1    8700 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 3500 8700 3600
Wire Wire Line
	8700 3500 8950 3500
Text GLabel 8950 3200 0    50   Output ~ 0
RS485EN
Wire Wire Line
	10100 3500 10650 3500
Wire Wire Line
	10650 3500 10650 3400
$Comp
L power:VCC #PWR0137
U 1 1 5FADD002
P 10650 3400
F 0 "#PWR0137" H 10650 3250 50  0001 C CNN
F 1 "VCC" H 10665 3573 50  0000 C CNN
F 2 "" H 10650 3400 50  0001 C CNN
F 3 "" H 10650 3400 50  0001 C CNN
	1    10650 3400
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP6
U 1 1 5F9BEC14
P 9950 4300
F 0 "JP6" H 9950 4505 50  0000 C CNN
F 1 "SolderJumper_COOL_OR_HEAT" H 9950 4414 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 9950 4300 50  0001 C CNN
F 3 "~" H 9950 4300 50  0001 C CNN
	1    9950 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5F9BEC1A
P 9100 4300
F 0 "R21" V 8893 4300 50  0000 C CNN
F 1 "10k" V 8984 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9030 4300 50  0001 C CNN
F 3 "~" H 9100 4300 50  0001 C CNN
	1    9100 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 4300 9300 4300
Wire Wire Line
	9300 4300 9300 4000
Wire Wire Line
	9300 4000 8950 4000
Connection ~ 9300 4300
Wire Wire Line
	9300 4300 9800 4300
$Comp
L power:GND #PWR0138
U 1 1 5F9BEC25
P 8700 4400
F 0 "#PWR0138" H 8700 4150 50  0001 C CNN
F 1 "GND" H 8705 4227 50  0000 C CNN
F 2 "" H 8700 4400 50  0001 C CNN
F 3 "" H 8700 4400 50  0001 C CNN
	1    8700 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4300 8700 4400
Wire Wire Line
	8700 4300 8950 4300
Text GLabel 8950 4000 0    50   Output ~ 0
COOL_OR_HEAT
Wire Wire Line
	10100 4300 10650 4300
Wire Wire Line
	10650 4300 10650 4200
$Comp
L power:VCC #PWR0139
U 1 1 5F9BEC30
P 10650 4200
F 0 "#PWR0139" H 10650 4050 50  0001 C CNN
F 1 "VCC" H 10665 4373 50  0000 C CNN
F 2 "" H 10650 4200 50  0001 C CNN
F 3 "" H 10650 4200 50  0001 C CNN
	1    10650 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV4
U 1 1 5F9E23EF
P 7600 3000
F 0 "RV4" V 7393 3000 50  0000 C CNN
F 1 "R_POT" V 7484 3000 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Alps_RK163_Single_Horizontal" H 7600 3000 50  0001 C CNN
F 3 "~" H 7600 3000 50  0001 C CNN
	1    7600 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0140
U 1 1 5F9E23F5
P 7250 3050
F 0 "#PWR0140" H 7250 2800 50  0001 C CNN
F 1 "GND" H 7255 2877 50  0000 C CNN
F 2 "" H 7250 3050 50  0001 C CNN
F 3 "" H 7250 3050 50  0001 C CNN
	1    7250 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3000 7250 3000
Wire Wire Line
	7250 3000 7250 3050
$Comp
L power:VCC #PWR0141
U 1 1 5F9E23FD
P 7950 2950
F 0 "#PWR0141" H 7950 2800 50  0001 C CNN
F 1 "VCC" H 7965 3123 50  0000 C CNN
F 2 "" H 7950 2950 50  0001 C CNN
F 3 "" H 7950 2950 50  0001 C CNN
	1    7950 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3000 7950 3000
Wire Wire Line
	7950 3000 7950 2950
Text GLabel 7450 3400 0    50   Input ~ 0
TRIM3
Wire Wire Line
	7600 3400 7450 3400
Wire Wire Line
	7600 3150 7600 3400
$Comp
L Device:R_POT RV3
U 1 1 5FADF050
P 7600 2200
F 0 "RV3" V 7393 2200 50  0000 C CNN
F 1 "R_POT" V 7484 2200 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Runtron_RM-065_Vertical" H 7600 2200 50  0001 C CNN
F 3 "RKT6V-10K" H 7600 2200 50  0001 C CNN
	1    7600 2200
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5FAEE7A6
P 2750 6350
F 0 "R25" V 2543 6350 50  0000 C CNN
F 1 "10k" V 2634 6350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2680 6350 50  0001 C CNN
F 3 "~" H 2750 6350 50  0001 C CNN
	1    2750 6350
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR0145
U 1 1 5FB00431
P 2400 6150
F 0 "#PWR0145" H 2400 6000 50  0001 C CNN
F 1 "VCC" H 2415 6323 50  0000 C CNN
F 2 "" H 2400 6150 50  0001 C CNN
F 3 "" H 2400 6150 50  0001 C CNN
	1    2400 6150
	1    0    0    -1  
$EndComp
Text GLabel 1900 4900 0    50   Input ~ 0
RX1
Text GLabel 1900 4800 0    50   Output ~ 0
TX1
Wire Wire Line
	2000 4900 1900 4900
Wire Wire Line
	2000 4800 1900 4800
Text GLabel 1800 2800 0    50   Input ~ 0
RX2
Text GLabel 1800 2700 0    50   Output ~ 0
TX2
Wire Wire Line
	2000 2700 1800 2700
Wire Wire Line
	2000 2800 1800 2800
Text GLabel 2800 6150 0    50   Input ~ 0
RESET
Text GLabel 1800 3000 0    50   Input ~ 0
TRIM1
Text GLabel 1800 3200 0    50   Input ~ 0
TRIM3
Text GLabel 1800 2900 0    50   Input ~ 0
CTRL_SIG_ADC
Text GLabel 1800 2600 0    50   Input ~ 0
TEMP_ADC
Text GLabel 1800 2500 0    50   Input ~ 0
PRESSURE_ADC
Wire Wire Line
	2000 2900 1800 2900
Wire Wire Line
	2000 2600 1800 2600
Wire Wire Line
	2000 2500 1800 2500
Wire Wire Line
	2000 3000 1800 3000
Wire Wire Line
	2000 3100 1800 3100
Wire Wire Line
	2000 3200 1800 3200
Text GLabel 1900 4200 0    50   Input ~ 0
ZC_DETECTION
Text GLabel 1900 4300 0    50   Output ~ 0
TRIAC_CONTROL
Text GLabel 1900 5400 0    50   Input ~ 0
TRIM_OR_CTRLSIG
Text GLabel 1900 5300 0    50   Input ~ 0
AUTO_OR_EXTERNAL
Text GLabel 1750 4400 0    50   Input ~ 0
RS485EN
Wire Wire Line
	2000 4200 1900 4200
Wire Wire Line
	2000 4300 1900 4300
Wire Wire Line
	2000 5500 1900 5500
Wire Wire Line
	2000 5400 1900 5400
Wire Wire Line
	2000 5300 1900 5300
Text GLabel 1800 3300 0    50   Output ~ 0
LED_R
Wire Wire Line
	2000 3300 1800 3300
Wire Wire Line
	2000 3400 1800 3400
Wire Wire Line
	2000 3500 1800 3500
Text GLabel 1900 5200 0    50   Input ~ 0
COOL_OR_HEAT
Text GLabel 1900 4700 0    50   Input ~ 0
DE_RE
Wire Wire Line
	2000 4700 1900 4700
$Comp
L Device:R R31
U 1 1 5FB36AA2
P 10000 5650
F 0 "R31" V 9793 5650 50  0000 C CNN
F 1 "220R" V 9884 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9930 5650 50  0001 C CNN
F 3 "~" H 10000 5650 50  0001 C CNN
	1    10000 5650
	0    1    1    0   
$EndComp
$Comp
L Device:R R32
U 1 1 5FB36EE8
P 10000 6150
F 0 "R32" V 9793 6150 50  0000 C CNN
F 1 "220R" V 9884 6150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 9930 6150 50  0001 C CNN
F 3 "~" H 10000 6150 50  0001 C CNN
	1    10000 6150
	0    1    1    0   
$EndComp
$Comp
L Device:C C13
U 1 1 5FABF930
P 5750 4350
F 0 "C13" V 5498 4350 50  0000 C CNN
F 1 "100n" V 5589 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5788 4200 50  0001 C CNN
F 3 "~" H 5750 4350 50  0001 C CNN
	1    5750 4350
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C15
U 1 1 5FD15F3C
P 6200 4350
F 0 "C15" H 6318 4396 50  0000 L CNN
F 1 "4,7u tantal" H 6318 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6238 4200 50  0001 C CNN
F 3 "TAJR105M010RNJ" H 6200 4350 50  0001 C CNN
	1    6200 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4100 6200 4200
Wire Wire Line
	5750 4100 5750 4200
Wire Wire Line
	5750 4100 6200 4100
Wire Wire Line
	6200 4550 6200 4500
Wire Wire Line
	5750 4500 5750 4550
Wire Wire Line
	5750 4550 6200 4550
$Comp
L power:GND #PWR0153
U 1 1 5FD899D5
P 6200 4700
F 0 "#PWR0153" H 6200 4450 50  0001 C CNN
F 1 "GND" H 6205 4527 50  0000 C CNN
F 2 "" H 6200 4700 50  0001 C CNN
F 3 "" H 6200 4700 50  0001 C CNN
	1    6200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 4550 6200 4700
Connection ~ 6200 4550
Wire Wire Line
	2900 6350 2950 6350
Wire Wire Line
	2800 6150 2950 6150
Wire Wire Line
	2950 6150 2950 6350
Connection ~ 2950 6350
Wire Wire Line
	2950 6350 3200 6350
Wire Wire Line
	2600 6350 2400 6350
Wire Wire Line
	2400 6350 2400 6150
$Comp
L Device:C C12
U 1 1 5FE82D57
P 2400 6650
F 0 "C12" V 2148 6650 50  0000 C CNN
F 1 "100n" V 2239 6650 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2438 6500 50  0001 C CNN
F 3 "~" H 2400 6650 50  0001 C CNN
	1    2400 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 6350 2400 6500
Connection ~ 2400 6350
$Comp
L power:GND #PWR0155
U 1 1 5FE94FD1
P 2400 6900
F 0 "#PWR0155" H 2400 6650 50  0001 C CNN
F 1 "GND" H 2405 6727 50  0000 C CNN
F 2 "" H 2400 6900 50  0001 C CNN
F 3 "" H 2400 6900 50  0001 C CNN
	1    2400 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 6800 2400 6900
Text GLabel 1600 4000 0    50   Input ~ 0
ALTERNATIVE_BOOT
$Comp
L Jumper:SolderJumper_2_Open JP8
U 1 1 5FFAB1D7
P 4500 6700
F 0 "JP8" H 4500 6905 50  0000 C CNN
F 1 "SolderJumper_RESET" H 4500 6814 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4500 6700 50  0001 C CNN
F 3 "~" H 4500 6700 50  0001 C CNN
	1    4500 6700
	-1   0    0    1   
$EndComp
Text GLabel 4800 6700 2    50   Output ~ 0
RESET
Wire Wire Line
	4650 6700 4800 6700
Wire Wire Line
	4350 6700 4050 6700
Wire Wire Line
	4050 6700 4050 6900
$Comp
L power:GND #PWR0158
U 1 1 5FFD1EAF
P 4050 6900
F 0 "#PWR0158" H 4050 6650 50  0001 C CNN
F 1 "GND" H 4055 6727 50  0000 C CNN
F 2 "" H 4050 6900 50  0001 C CNN
F 3 "" H 4050 6900 50  0001 C CNN
	1    4050 6900
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0159
U 1 1 60039D9C
P 8150 5150
F 0 "#PWR0159" H 8150 5000 50  0001 C CNN
F 1 "VCC" H 8165 5323 50  0000 C CNN
F 2 "" H 8150 5150 50  0001 C CNN
F 3 "" H 8150 5150 50  0001 C CNN
	1    8150 5150
	1    0    0    -1  
$EndComp
Text GLabel 7350 5650 0    50   Output ~ 0
SWCLK
$Comp
L power:GND #PWR0160
U 1 1 60089D4A
P 7050 5900
F 0 "#PWR0160" H 7050 5650 50  0001 C CNN
F 1 "GND" H 7055 5727 50  0000 C CNN
F 2 "" H 7050 5900 50  0001 C CNN
F 3 "" H 7050 5900 50  0001 C CNN
	1    7050 5900
	1    0    0    -1  
$EndComp
Text GLabel 7450 5850 0    50   Output ~ 0
SWDIO
Text GLabel 7450 5950 0    50   Input ~ 0
RESET
$Comp
L Device:R R30
U 1 1 6033E3A4
P 7950 6150
F 0 "R30" V 7743 6150 50  0000 C CNN
F 1 "10k" V 7834 6150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7880 6150 50  0001 C CNN
F 3 "~" H 7950 6150 50  0001 C CNN
	1    7950 6150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8150 6150 8150 5650
Wire Wire Line
	8150 5650 8200 5650
$Comp
L power:GND #PWR0161
U 1 1 60381EA6
P 7700 6250
F 0 "#PWR0161" H 7700 6000 50  0001 C CNN
F 1 "GND" H 7705 6077 50  0000 C CNN
F 2 "" H 7700 6250 50  0001 C CNN
F 3 "" H 7700 6250 50  0001 C CNN
	1    7700 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R29
U 1 1 603921FC
P 7850 5400
F 0 "R29" V 7643 5400 50  0000 C CNN
F 1 "10k" V 7734 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7780 5400 50  0001 C CNN
F 3 "~" H 7850 5400 50  0001 C CNN
	1    7850 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7350 5650 8150 5650
Connection ~ 8150 5650
Wire Wire Line
	7050 5750 8200 5750
Wire Wire Line
	7450 5850 7850 5850
Wire Wire Line
	7450 5950 8200 5950
Wire Wire Line
	7850 5550 7850 5850
Connection ~ 7850 5850
Wire Wire Line
	7850 5850 8200 5850
Wire Wire Line
	8200 5550 8150 5550
Wire Wire Line
	8150 5550 8150 5200
Connection ~ 8150 5200
Wire Wire Line
	8150 5200 8150 5150
Wire Wire Line
	7850 5250 7850 5200
Wire Wire Line
	7850 5200 8150 5200
Wire Wire Line
	7050 5750 7050 5900
Text GLabel 1800 3500 0    50   Output ~ 0
LED_Y
Text GLabel 1800 3400 0    50   Output ~ 0
LED_G
Text GLabel 1800 5100 0    50   Input ~ 0
SDA
Text GLabel 1800 5000 0    50   Input ~ 0
SCL
Wire Wire Line
	1800 5000 2000 5000
Wire Wire Line
	1800 5100 2000 5100
$Comp
L MCU_ST_STM32G0:STM32G030C8T6 U3
U 1 1 5FA95420
P 2000 2500
F 0 "U3" H 3500 2850 50  0000 C CNN
F 1 "STM32G030C8Tx" H 3500 2750 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 1400 1100 50  0001 R CNN
F 3 "https://pl.mouser.com/datasheet/2/389/stm32g030c6-1826662.pdf" H 2000 2500 50  0001 C CNN
	1    2000 2500
	1    0    0    -1  
$EndComp
Wire Notes Line
	800  1800 6650 1800
Text Notes 800  2000 0    129  ~ 0
MICROCONTROLLER \n
Wire Wire Line
	7700 6150 7800 6150
Wire Wire Line
	7700 6150 7700 6250
Wire Notes Line
	9000 6500 6950 6500
Wire Notes Line
	6950 6500 6950 4700
Wire Notes Line
	6950 4700 9000 4700
Wire Notes Line
	9000 4700 9000 6500
Text Notes 6950 4900 0    129  ~ 0
FLASH AND DEBUG\n
Wire Notes Line
	9200 4700 9200 6500
Wire Notes Line
	9200 6500 11050 6500
Wire Notes Line
	11050 6500 11050 4700
Wire Notes Line
	11050 4700 9200 4700
Text Notes 9200 4900 0    129  ~ 0
UI\n
Wire Notes Line
	6950 4650 11050 4650
Wire Notes Line
	11050 4650 11050 600 
Wire Notes Line
	11050 600  6950 600 
Wire Notes Line
	6950 600  6950 4650
Text Notes 7000 800  0    129  ~ 0
CONFIGURATION\n
$Sheet
S 2750 600  1500 650 
U 5FC6AEA7
F0 "conectivity" 50
F1 "connectivity.sch" 50
$EndSheet
Text GLabel 4450 6400 2    50   Output ~ 0
ALTERNATIVE_BOOT
$Comp
L Jumper:SolderJumper_2_Open JP7
U 1 1 5FEA8C0D
P 4250 6400
F 0 "JP7" H 4250 6605 50  0000 C CNN
F 1 "SolderJumper_BOOT" H 4250 6514 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4250 6400 50  0001 C CNN
F 3 "~" H 4250 6400 50  0001 C CNN
	1    4250 6400
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 6150 8100 6150
Wire Wire Line
	4050 6400 4050 6250
$Comp
L power:VCC #PWR0132
U 1 1 5FFD098B
P 4050 6250
F 0 "#PWR0132" H 4050 6100 50  0001 C CNN
F 1 "VCC" H 4065 6423 50  0000 C CNN
F 2 "" H 4050 6250 50  0001 C CNN
F 3 "" H 4050 6250 50  0001 C CNN
	1    4050 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 6400 4050 6400
Wire Wire Line
	4400 6400 4450 6400
$Comp
L Device:C C7
U 1 1 60079262
P 5100 4950
F 0 "C7" V 4848 4950 50  0000 C CNN
F 1 "100n" V 4939 4950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5138 4800 50  0001 C CNN
F 3 "~" H 5100 4950 50  0001 C CNN
	1    5100 4950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5000 4700 5100 4700
Wire Wire Line
	5100 4700 5100 4800
$Comp
L power:GND #PWR0133
U 1 1 600BAC91
P 5450 5300
F 0 "#PWR0133" H 5450 5050 50  0001 C CNN
F 1 "GND" H 5455 5127 50  0000 C CNN
F 2 "" H 5450 5300 50  0001 C CNN
F 3 "" H 5450 5300 50  0001 C CNN
	1    5450 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 5100 5100 5200
Wire Wire Line
	5000 4500 5150 4500
Wire Wire Line
	5150 4500 5150 4100
Wire Wire Line
	5150 4100 5000 4100
Connection ~ 5750 4100
Wire Wire Line
	6200 4100 6200 3950
Connection ~ 6200 4100
$Comp
L power:VCC #PWR0134
U 1 1 60105455
P 6200 3950
F 0 "#PWR0134" H 6200 3800 50  0001 C CNN
F 1 "VCC" H 6215 4123 50  0000 C CNN
F 2 "" H 6200 3950 50  0001 C CNN
F 3 "" H 6200 3950 50  0001 C CNN
	1    6200 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3900 5150 3900
Text GLabel 5150 3900 2    50   Input ~ 0
NRST_PIN
Text GLabel 3200 6350 2    50   Input ~ 0
NRST_PIN
Text GLabel 1600 3900 0    50   Input ~ 0
SWCLK
Wire Wire Line
	2000 3900 1650 3900
Wire Wire Line
	1650 3900 1650 4000
Wire Wire Line
	1650 4000 1600 4000
Connection ~ 1650 3900
Wire Wire Line
	1650 3900 1600 3900
Text GLabel 1600 3800 0    50   BiDi ~ 0
SWDIO
Wire Wire Line
	2000 3800 1600 3800
Wire Wire Line
	2000 5200 1900 5200
Wire Wire Line
	1750 4400 2000 4400
Wire Wire Line
	2000 4600 1900 4600
Text GLabel 1900 4600 0    50   Output ~ 0
SPI_CS
Text GLabel 1800 3100 0    50   Input ~ 0
TRIM2
Wire Wire Line
	2000 3600 1800 3600
Wire Wire Line
	2000 3700 1800 3700
Text GLabel 1800 3600 0    50   BiDi ~ 0
SPI_MISO
Text GLabel 1800 3700 0    50   BiDi ~ 0
SPI_MOSI
Text GLabel 1900 4500 0    50   Output ~ 0
SPI_SCK
Wire Wire Line
	2000 4500 1900 4500
NoConn ~ 5000 2500
NoConn ~ 5000 2600
NoConn ~ 5000 2700
NoConn ~ 5000 3600
NoConn ~ 5000 3700
Wire Wire Line
	5000 4300 5450 4300
Wire Wire Line
	5450 4300 5450 5200
Wire Wire Line
	5150 4100 5750 4100
Connection ~ 5150 4100
Wire Wire Line
	5100 5200 5450 5200
Wire Wire Line
	5450 5200 5450 5300
Connection ~ 5450 5200
Wire Notes Line
	6650 7400 800  7400
Wire Notes Line
	6650 1800 6650 7400
Wire Notes Line
	800  1800 800  7400
NoConn ~ 5000 2800
NoConn ~ 5000 2900
$Comp
L Connector:Conn_01x05_Female J8
U 1 1 5FBE4EC8
P 8400 5750
F 0 "J8" H 8428 5776 50  0000 L CNN
F 1 "Conn_01x05_Female" H 8428 5685 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 8400 5750 50  0001 C CNN
F 3 "~" H 8400 5750 50  0001 C CNN
	1    8400 5750
	1    0    0    -1  
$EndComp
Text GLabel 1900 5500 0    50   Input ~ 0
TEMP_OR_PRESS
NoConn ~ 2000 4000
NoConn ~ 5000 3400
NoConn ~ 5000 3300
NoConn ~ 5000 3200
NoConn ~ 5000 3100
NoConn ~ 2000 5600
NoConn ~ 2000 5700
$EndSCHEMATC
